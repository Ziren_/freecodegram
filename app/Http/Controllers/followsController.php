<?php

namespace freeCodeGram\Http\Controllers;

use freeCodeGram\User;
use Illuminate\Http\Request;

class followsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function store(User $user){
        return auth()->user()->following()->toggle($user->profile);
    }
}
