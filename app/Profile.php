<?php

namespace freeCodeGram;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function profileImage(){
        $imagePath= ($this->image) ? $this->image : 'profile/46493699_2191322934415346_9190760088183242752_n.jpg';
        return '/storage/'. $imagePath;
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    protected $fillable = [
        'title', 'description', 'url', 'image',
    ];
    public function followers(){
        return $this->belongsToMany(User::class);
    }
}
